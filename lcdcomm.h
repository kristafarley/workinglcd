/* 
 * File:   lcdcomm.h
 * Author: Nacole
 *
 * Created on February 12, 2024, 2:44 AM
 */

// Need A2 (CS) to go low
//then input serial data by bytes using C5
//then raise A2 to end Tx
//then push data to LCD

#include <stdint.h>

#define IO_EXPD_ADDRESS         0x40 //SSP1BUF 
#define GPIO_A_ADDRESS          0x12 
#define GPIO_B_ADDRESS          0x13 
#define IO_DIR_B_ADDRESS        0x01
#define LCD_SET_DDRAM_ADDRESS   0x80 
#define LINE1_START_ADDRESS     0x80
#define LINE2_START_ADDRESS     0xC0

#define LCD_CLEAR               0x01 //00000001 - goes in GPIOB to CLEAR DISPLAY
#define LCD_VDD_EN              0x20 //00100000 - goes in GPIOA - 
#define LCD_FUNCTION_SET        0x3C //00111100
#define LCD_SET_DISPLAY         0x0C //00001100


#define _XTAL_FREQ 500000


void LCD_Initialize(void);
void LCD_WriteIOExpander(uint8_t reg, uint8_t data);
void LCD_WriteCommand(uint8_t cmd);
void LCD_WriteByte(uint8_t data);
void LCD_WriteString(const uint8_t *data, int length);
void LCD_GoTo(uint8_t row, uint8_t column);
void LCD_Clear(void);

