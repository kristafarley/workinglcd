/* 
 * File:   sensorCodes.h
 * Author: glarson
 *
 * Created on January 31, 2024, 7:27 PM
 */

#define PWM_Signal  LATGbits.LATG3 // PWM Output RG3 - Edited
#define PWM_Bool    LATGbits.LATG1

/* Application specific user parameters */
#define POT_CHAN_1    (15)
#define POT_CHAN_2    (14)
#define PERIOD      (75)
#define OFFSET      (25)
#define _XTAL_FREQ 500000

extern int sharedTrack;

// Function Prototypes
// Helpers
void LEDs(int a,int b,int c,int d,int er,int f,int g,int h, int eg);
void trainSpeed (int a);
void setRelays (int a);

// Sensor Codes
void Code_0 (void);
void Code_63(void);
void Code_95(void);
void Code_119(void);
void Code_125(void);
void Code_127(void);
void Code_175(void);
void Code_183(void);
void Code_187(void);
void Code_190(void);
void Code_191(void);
void Code_207(void);
void Code_215(void);
void Code_219(void);
void Code_222(void);
void Code_223(void);
void Code_231(void);
void Code_237(void);
void Code_239(void);
void Code_243(void);
void Code_245(void);
void Code_246(void);
void Code_247(void);
void Code_249(void);
void Code_251(void);
void Code_252(void);
void Code_253(void);
void Code_254(void);
void Code_255(void);

