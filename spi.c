#include "spi.h"
#include <xc.h>
//Need to set up STAT, BUF (via STAT), and CON
//low 6 bits of STAT are read-only, top two are R/W


typedef struct { 
    unsigned char con1; //con1 == SSPxCON1 - - 00001010 - 
    unsigned char stat; //stat == SSPxSTAT // - 01000000 - falling clock edge trigger
    unsigned char add;//add == SSPxADD //  
    unsigned char operation;//operation == Master/Slave //
} spi1_configuration_t;

void SPI1_Initialize(void)
{
    /*Setup PPS Pins
    
    //SPI setup*/
    SSP1STAT = 0xC0; //C0 - 11000000 - master fill, falling clock edge trigger
    SSP1CON1 = 0x30; //30 - 00110000 - enables SPI pins, sets active low clock - note to self, was 30, changing to 64
    SSP1CON2 = 0x64;
    TRISCbits.TRISC3 = 0; // set C3 (serial clock) to output
    SSP1CON1bits.SSPEN = 0; //
}

static const spi1_configuration_t spi1_configuration[] = {   
    { 0xC0, 0x30, 0 }
};
// Port Configuration
/*
ANSELA = 0x00; // Digital I/O;
ANSELB = 0x00; // Digital I/O;
RB1PPS = 0x1E; // RB1->SPI1:SCK1; -- output;
RB3PPS = 0x1F; // RB3->SPI1:SDO1; -- output;
RA3PPS = 0x20; // RA3->SPI1:SS1; -- output;
*/

//con1 == SSPxCON1, stat == SSPxSTAT, add == SSPxADD, operation == Master/Slave
static const spi1_configuration_t spi1_configuration[] = {   
    { 0x1a, 0x0, 0x1, 0 }

bool SPI1_Open(spi1_modes_t spi1_configuration_t)
{
    if(!SSP1CON1bits.SSPEN)
    {
        SSP1STAT = spi1_configuration[spi1UniqueConfiguration].stat;
        SSP1CON1 = spi1_configuration[spi1UniqueConfiguration].con1;
        TRISCbits.TRISC3 = spi1_configuration[spi1UniqueConfiguration].operation;
        SSP1CON1bits.SSPEN = 1;
        return true;
    }
    return false;
}

void SPI1_Close(void)
{
    SSP1CON1bits.SSPEN = 0;
}

unsigned char SPI1_ExchangeByte(unsigned char data)
{
    SSP1BUF = data;
    while(!PIR1bits.SSP1IF);
    PIR1bits.SSP1IF = 0;
    return SSP1BUF;
}

void SPI1_ExchangeBlock(void *block, size_t blockSize)
{
    uint8_t *data = block;
    while(blockSize--)
    {
        SSP1BUF = *data;
        while(!PIR1bits.SSP1IF);
        PIR1bits.SSP1IF = 0;
        *data++ = SSP1BUF;
    }
}

// Half Duplex SPI Functions
void SPI1_WriteBlock(void *block, size_t blockSize)
{
    uint8_t *data = block;
    while(blockSize--)
    {
        SPI1_ExchangeByte(*data++);
    }
}

void SPI1_WriteByte(uint8_t byte)
{
    SSP1BUF = byte;
}
