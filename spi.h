/* 
 * File:   spi.h
 * Author: Nacole
 *
 * Created on February 26, 2024, 1:01 AM
 */

#ifndef SPI1_H
#define SPI1_H

/**
  Section: Included Files
*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* SPI interfaces */
typedef enum { 
    SPI1_DEFAULT
} spi1_modes_t;

void SPI1_Initialize(void);
bool SPI1_Open(spi1_modes_t spi1_configuration_t);
void SPI1_Close(void);
unsigned char SPI_Exchange8bit(unsigned char data);
void SPI1_ExchangeBlock(void *block, size_t blockSize);
void SPI1_WriteBlock(void *block, size_t blockSize);
void SPI1_WriteByte(uint8_t byte);
#endif //SPI1_H

