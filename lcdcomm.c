
#include <xc.h> //
#include <pic18f87j11.h> //
#include "lcdcomm.h" //
#include "spi.h" //
#include "pin_manager.h"
#include <stdint.h> //


uint8_t SPI_Exchange8bit(uint8_t data) {
 TRISDbits.TRISD7 = 1; // slave select input
 SSP1CON1 = 0x20; // active low clock signal, enable serial port
 SSP1STATbits.SMP = 1; //SPI Master mode sample at end
 SSP1STATbits.CKE = 0; //transmit on the falling edge
 SSP1BUF = data;
    while(!PIR1bits.SSP1IF);
    PIR1bits.SSP1IF = 0;
    return SSP1BUF;
}

void LCD_WriteIOExpander(uint8_t reg, uint8_t data) { 
    LCD_CHIP_SELECT_SetLow();//allow A2 to receive data
    __delay_ms (1);
    SPI_Exchange8bit(IO_EXPD_ADDRESS);
    __delay_ms (1);
    SPI_Exchange8bit(reg);
    __delay_ms(1);
    SPI_Exchange8bit(data);
    __delay_ms(1);
    LCD_CHIP_SELECT_SetHigh();//set A2 to push data out
    __delay_ms(1);
}

void LCD_WriteCommand(uint8_t cmd) {
    LCD_WriteIOExpander(GPIO_A_ADDRESS, 0x60);  //01100000 -RS LOW (Command) -- E HIGH -- Pulse Enabled 
    LCD_WriteIOExpander(GPIO_B_ADDRESS, cmd);   //Write the command on PORT B
    LCD_WriteIOExpander(GPIO_A_ADDRESS, 0x20);  //00100000 -RS LOW (Command)-- E LOW -- Pulse Disabled
}

void LCD_WriteByte(uint8_t data) {
    LCD_WriteIOExpander(GPIO_A_ADDRESS, 0xE0);  //11110000 -RS HIGH (Data)-- Pulse Enabled
    LCD_WriteIOExpander(GPIO_B_ADDRESS, data);   //Write the byte on PORT B
    LCD_WriteIOExpander(GPIO_A_ADDRESS, 0xA0);  //10100000 -RS HIGH --  -- Pulse Disabled // latch the data
}

void LCD_WriteString(const uint8_t *data, int length)
{// takes array and puts in, ends once length of string is hit
    uint8_t i = 0;
    while (i<=length) {
        LCD_WriteByte(data[i++]);
    }}

void LCD_GoTo(uint8_t row, uint8_t column){
    unsigned char pos;
    if (row <2) {
       uint8_t pos = (row==0)? (LINE1_START_ADDRESS | column):(LINE2_START_ADDRESS | column);
       LCD_WriteCommand(pos);
    }
}

void LCD_Clear(){
    LCD_WriteCommand(LCD_CLEAR);
    LCD_GoTo(0,0);
}

void SPI1_Initialize(void)
{
    /*Setup PPS Pins
    
    //SPI setup*/
    SSP1STAT = 0xC0; //C0 - 11000000 - master fill, falling clock edge trigger
    SSP1CON1 = 0x64; //30 - 00110000 - enables SPI pins, sets active low clock - note to self, was 30, changing to 64
    SSP1CON2 = 0x64;
    TRISCbits.TRISC3 = 0; // set C3 (serial clock) to output
    SSP1CON1bits.SSPEN = 0; //
}

