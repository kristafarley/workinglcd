/*
 * File:   adc.c
 * Author: Dr. Ron Hayne
 *
 * Revision history: Created 10/8/17
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <xc.h>
#include <pic18f87J11.h>      /* Defines special function registers, CP0 regs  */
#include "user.h"            /* variables/params used by user.c               */
#include "adc.h"
#include"sensorCodes.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void Init_ADC(void)
{
    /*ADCON0 = 0x01; // Channel 0, ADC enabled
    ADCON1 = 0x0E; // Vdd, Vss, 1 analog channel
    ADCON2 = 0x2D; // Left Justified, Fosc/16, 12 Tad*/
    
    ADCON0 = 0x3D;
    ADCON1 = 0x2D;
}

unsigned char Read_ADC (unsigned char Channel_Number)
{
    // Select channel to convert
    ADCON0bits.CHS = Channel_Number;
    // Start conversion
    ADCON0bits.GO = 1;
    // Wait for completion
    while (ADCON0bits.NOT_DONE == 1);
    // Return converted data
    return ADRESH;
}

