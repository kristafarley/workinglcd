// Include Files
#include <xc.h>
#include <pic18f87j11.h>      /* Defines special function registers, CP0 regs  */
#include "user.h"            /* variables/params used by user.c               */
#include "adc.h"
#include "sensorCodes.h"

int sharedTrack;

/****************************************************************
 * Helper Functions                                             *
 ****************************************************************/
void LEDs(int a,int b,int c,int d,int er,int f,int g,int h, int eg){    // ... int h, int mod){
    // Activate appropriate LEDs
    LATCbits.LATC2 = a;     // LED A
    LATCbits.LATC6 = b;     // LED B
    LATCbits.LATC4 = c;     // LED C
    LATCbits.LATC7 = d;     // LED D
    LATBbits.LATB7 = er;    // LED E Red
    LATBbits.LATB6 = f;     // LED F
    LATBbits.LATB5 = g;     // LED G
    LATBbits.LATB4 = h;     // LED H
    LATBbits.LATB3 = eg;    // LED E Green 
}

void trainSpeed (int a){
    /* a = 0 --> Stop Trains
     * a = 1 --> H1 = T1, H2 = T2
     * a = 2 --> H1 = T1, H2 = H3 = T2
     * a = 3 --> H1 = H3 = T1, H2 = T2
     * a = 4 --> H1 = H3 = T1, H2 = 0
     * a = 5 --> H1 = 0, H2 = H3 = T2
    */
    if (a == 0){            
        PORTGbits.RG1 = 0;                  // CCPR4 (Train 1)
        PORTGbits.RG2 = 0;                  // CCPR5 (Train 2)
        PORTEbits.RE0 = 0;                  // ECCP3 (Shared Track)
    }
    else if(a == 1){          
        PORTGbits.RG1 = 1;                  // CCPR4 (Train 1)
        PORTGbits.RG2 = 1;                  // CCPR5 (Train 2)
        // PORTEbits.RE0 = 0;               // ECCP3 (Shared Track) - Not set for now
        CCPR4 = speed_1 + OFFSET;           // H1 = T1 user speed
        CCPR5 = speed_2 + OFFSET;           // H2 = T2 user speed
        // CCPR3 = speed_2 + OFFSET;        // H3 = T2 user speed - Not set for now
    }
    else if(a == 2){          
        PORTGbits.RG1 = 1;                  // CCPR4 (Train 1)
        PORTGbits.RG2 = 1;                  // CCPR5 (Train 2)
        PORTEbits.RE0 = 1;                  // ECCP3 (Shared Track)
        CCPR4 = speed_1 + OFFSET;           // H1 = T1 user speed
        CCPR5 = speed_2 + OFFSET;           // H2 = T2 user speed
        CCPR3 = speed_2 + OFFSET;           // H3 = T2 user speed 
    }
    else if(a == 3){          
        PORTGbits.RG1 = 1;                  // CCPR4 (Train 1)
        PORTGbits.RG2 = 1;                  // CCPR5 (Train 2)
        PORTEbits.RE0 = 1;                  // ECCP3 (Shared Track)
        CCPR4 = speed_1 + OFFSET;           // H1 = T1 user speed
        CCPR5 = speed_2 + OFFSET;           // H2 = T2 user speed
        CCPR3 = speed_1 + OFFSET;           // H3 = T1 user speed 
    }
    else if (a == 4){
        CCPR4 = speed_1 + OFFSET;           // H1 = T1 user speed
        CCPR5 = 0;                          // H2 = 0
        CCPR3 = speed_1 + OFFSET;           // H3 = T1 user speed
    }
    else if (a == 5){
        CCPR4 = 0;                          // H1 = 0
        CCPR5 = speed_2 + OFFSET;           // H2 = T2 user speed 
        CCPR3 = speed_2 + OFFSET;           // H3 = T2 user speed
    }
}

void setRelays (int a){
    /* a = 0 --> T2 entering / exiting
     * a = 1 --> T1 entering / exiting
     * a = 2 --> T1 entering, T2 exiting
     * a = 3 --> T1 exiting, T2 entering
     * confirm that relay signals are correct
    */
    if (a == 0){
        LATJbits.LATJ2 = 1; // Relay signals on
        LATJbits.LATJ0 = 1;
        __delay_ms(10);     // Wait 10 ms
        LATJbits.LATJ2 = 0; // Relay signals off
        LATJbits.LATJ0 = 0;
    }
    if (a == 1){
        LATJbits.LATJ3 = 1; // Relay signals on
        LATJbits.LATJ1 = 1;
        __delay_ms(10);     // Wait 10 ms
        LATJbits.LATJ3 = 0; // Relay signals off
        LATJbits.LATJ1 = 0;  //Set Relays for Train 1 
    }
    if (a == 2){
        LATJbits.LATJ3 = 1; // Relay signals on
        LATJbits.LATJ0 = 1;
        __delay_ms(10);     // Wait 10 ms
        LATJbits.LATJ3 = 0; // Relay signals off
        LATJbits.LATJ0 = 0;
    }
    if (a == 3){
        LATJbits.LATJ1 = 1; // Relay signals on
        LATJbits.LATJ2 = 1;
        __delay_ms(10);     // Wait 10 ms
        LATJbits.LATJ1 = 0; // Relay signals off
        LATJbits.LATJ2 = 0;
    }
}

/****************************************************************
 * Sensor Code Functions                                        *
 ****************************************************************/
void Code_0 (void){
    // Turn on all LEDs
    LEDs(1,1,1,1,1,1,1,1,0);
    // Stop trains
    trainSpeed(0);
}

void Code_63(void){
    LEDs(1,1,0,0,0,0,0,0,0);
    trainSpeed(1);
    sharedTrack = 0;
}

void Code_95(void){
    LEDs(1,0,1,0,0,0,0,0,0);
    setRelays(0);
    trainSpeed(2);
    sharedTrack = 2;
}

void Code_119(void){
    LEDs(1,0,0,0,0,0,0,0,1);  // LED E for train 2
    setRelays(0);
    trainSpeed(2);
    sharedTrack = 2;
}

void Code_125(void){
    LEDs(1,0,0,0,0,0,1,0,0);
    setRelays(0);
    trainSpeed(1);
    sharedTrack = 0;
}

void Code_127(void){
    LEDs(1,0,0,0,0,0,0,0,0);
    setRelays(0);
    CCPR4 = speed_1 + OFFSET;  //H1 = T1 user speed
}

void Code_175(void){
    LEDs(0,1,0,1,0,0,0,0,0);
    setRelays(1);
    trainSpeed(3);
    sharedTrack = 1;
}

void Code_183(void){
    LEDs(0,1,0,0,1,0,0,0,0); // Set LED E for train 1
    setRelays(1);     
    trainSpeed(3);
    sharedTrack = 1;
}

void Code_187(void){
    LEDs(0,1,0,0,0,1,0,0,0);
    trainSpeed(1);
}

void Code_190(void){
    LEDs(0,1,0,0,0,0,0,1,0);
    trainSpeed(1);    
}

void Code_191(void){
    LEDs(0,1,0,0,0,0,0,0,0);
    setRelays(1);      
    CCPR5 = speed_2 + OFFSET;   // H2 = T2 user speed
}

void Code_207(void){
    LEDs(0,0,1,1,0,0,0,0,0);
    
    if (priority == 1){
        setRelays(1);
        trainSpeed(4);
         sharedTrack = 1;
    }

    if (priority == 2){
        setRelays(0);   
        trainSpeed(5);
        sharedTrack = 2;
    }
}

void Code_215(void){
    LEDs(0,0,1,0,1,0,0,0,0);  // LED E for Train 1
    trainSpeed(4);
    sharedTrack = 1;   
}

void Code_219(void){
    LEDs(0,0,1,0,0,1,0,0,0);
    setRelays(0);
    trainSpeed(1);
    sharedTrack = 2;
}

void Code_222(void){
    LEDs(0,0,1,0,0,0,0,1,0);
    setRelays(0);
    trainSpeed(1);
    sharedTrack = 2;    
}

void Code_223(void){
    LEDs(0,0,1,0,0,0,0,0,0);
    if (sharedTrack == 0){
        setRelays(0);
        trainSpeed(2);   
        sharedTrack = 2;
    }
    else
    {
        CCPR5 = 0; //H2 = 0
    }
}

// RELAY SHENANIGANS ABOVE THIS POINT

void Code_231(void){
    LEDs(0,0,0,1,0,0,0,0,1);
    setRelays(2);    
    trainSpeed(5);
    sharedTrack = 2;
}

void Code_237(void){
    LEDs(0,0,0,1,0,0,1,0,0);    
    setRelays(1);
    trainSpeed(3);
    sharedTrack = 1;
}

void Code_239(void){
    LEDs(0,0,0,1,0,0,0,0,0);
    
    if (sharedTrack == 0){
        setRelays(1);
        trainSpeed(3);
        sharedTrack = 1; 
        }
    else{
        CCPR4 = 0; //H1 = 0;
        }
}

void Code_243(void){
    LEDs(0,0,0,0,0,1,0,0,1); // LED E for Train 2
    setRelays(2);
    trainSpeed(2);
    sharedTrack = 2; 
}

void Code_245(void){
    LEDs(0,0,0,0,1,0,1,0,0); // LED E for train 1
    setRelays(3);
    trainSpeed(3);   
    sharedTrack = 1;     
}

void Code_246(void){
    LEDs(0,0,0,0,0,0,0,1,1);
    setRelays(2);
    trainSpeed(2);  
    sharedTrack = 2;
}

void Code_247(void){
    if (sharedTrack == 1){
        LEDs(0,0,0,0,1,0,0,0,0); // LED E Turns Red (Train 1)
    }
    else if(sharedTrack == 1){
        LEDs(0,0,0,0,0,0,0,0,1); // LED E Turns Green (Train 2)
    }
}

void Code_249(void){
    LEDs(0,0,0,0,0,1,1,0,0);
    trainSpeed(1);    
}

void Code_251(void){
    LEDs(0,0,0,0,0,1,0,0,0);
    trainSpeed(1); 
}

void Code_252(void){
    LEDs(0,0,0,0,0,0,1,1,0);
    trainSpeed(1);       
}

void Code_253(void){
    LEDs(0,0,0,0,0,0,1,0,0);
    trainSpeed(1);
}

void Code_254(void){
    LEDs(0,0,0,0,0,0,0,1,0);
    trainSpeed(1);
}

void Code_255(void){
    LEDs(0,0,0,0,0,0,0,0,0);
    trainSpeed(1);  
}
