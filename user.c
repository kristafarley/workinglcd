// Senior Design
// The Conductors
/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <xc.h>
#include <pic18f87J11.h>      /* Defines special function registers, CP0 regs  */
#include "user.h"            /* variables/params used by user.c               */
#include "adc.h"
#include "sensorCodes.h"
#include "lcdcomm.h"
#include <stdint.h>
#include "spi.h"
#include "pin_manager.h"

unsigned short speed_1;
unsigned short speed_2;
int priority;
int sensorCode[8];





/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/*
void Priority_Read(void){
    // Note we may need to turn off analog mode
    if (PORTCbits.RC1 > 0){         // Train 1 Priority
        LATCbits.LATC2 = 1;         // Turn on LED
    }
    else {                          // Train 2 Priority  
        LATCbits.LATC2 = 0;         // Turn off LED
    }
}
*/
/*
void Sensor_Read(void){
    // Note we may need to turn off analog mode
    if (PORTDbits.RD3 == 0){        // Covered
        LATCbits.LATC3 = 0;         // Turn on LED
    }
    else {                          // Uncovered
        LATCbits.LATC3 = 1;         // Turn off LED
    }
}
 */
void Init_GPIO(void) {
    /* Setup functionality and port direction */
    // LED output
    // Set directions to output
    TRISD = 0;
    TRISB = 0;
    TRISC = 0;
    TRISG = 0;
    TRISJ = 0;
    TRISE = 1;
  
    
    /* POSSIBLY DELETE
    // Turn off LEDs for initialization
    LEDS_D = 0;
    LEDS_B = 0;
     */

    // Inputs
    // TRISDbits.TRISD0 = 1; //Potentiometer input
    TRISDbits.TRISD3 = 1; //PhotoResistor input
    TRISCbits.TRISC1 = 1; //Toggle Switch input
    TRISHbits.TRISH7 = 1; // Potentiometer input 
 
    // Outputs
    TRISGbits.TRISG3 = 0; // PWM output
    TRISCbits.TRISC2 = 0; // Priority LED output
    TRISCbits.TRISC3 = 0; // MSSP SPI clock signal
    TRISAbits.TRISA2=0; // MSSP SPI CS signal
    TRISCbits.TRISC5=0; //MSSP SPI SI signal
    
    // Output LEDs Off
    LATCbits.LATC2 = 0;
    
    // Relay outputs off
    LATJbits.LATJ3 = 0; // Entry 1
    LATJbits.LATJ2 = 0; // Entry 2
    LATJbits.LATJ1 = 0; // Exit 1
    LATJbits.LATJ0 = 0; // Exit 2
    LATAbits.LATA2 = 1; // MSSP SPI CS(active low))
    
    
    //disable CVref to enable A2 digital output
    CM1CONbits.CREF=0;
    CM2CONbits.CREF=0;
}

void LCD_Initialize(void) {
    LCD_WriteIOExpander(IO_DIR_A_ADDRESS, OUTPUT_DIR); //set MPC port GPA to output
    LCD_WriteIOExpander(IO_DIR_B_ADDRESS,OUTPUT_DIR); //set MPC port GPB to output
    LCD_WriteIOExpander(GPIO_A_ADDRESS, LCD_VDD_EN); //enable the LCD
    __delay_ms(10);
    LCD_WriteCommand(LCD_FUNCTION_SET); // 00111100 - function set, 8-bit mode, 2-line, font
    __delay_ms(10);
    LCD_WriteCommand(LCD_CLEAR);
    __delay_ms(10);
    LCD_WriteCommand(LCD_SET_DISPLAY); //00001110
    __delay_ms(130);
    LCD_WriteCommand(LCD_SET_DDRAM_ADDRESS); // holds characters being displayed
    __delay_ms(1);
    LCD_GoTo(0,5);
    LCD_WriteString("  ||   ", 7);
    LCD_GoTo(1,0);
    LCD_WriteString("in/s   ||   in/s",16);
}

void Init_Timer2(void) {
    T2CON = 0; // Reset Timer2 to default
    T2CONbits.T2CKPS = 7; // divide clock by 256 with prescaler
    TMR2 = 0;
    // Set period for timer
    PR2 = PERIOD;    
    // Set initial duty cycle to 50%
    CCPR4 = PERIOD/2;
    CCPR5 = PERIOD/2;
    CCPR3 = PERIOD/2;
    // Configure CCP4 control register for PWM
    CCP4CON = 0x0C;
    // Start Timer 2
    T2CONbits.TMR2ON = 1;
}

void Init_App(void) {
    // Initialize peripherals
    Init_GPIO();
    Init_ADC();
    Init_Timer2();
    LCD_Initialize();
    SPI1_Initialize();
}

void UPDINT(void) {
    int pot_pos_1 = 0;//, speed_1 = 0;
    int pot_pos_2 = 0;//, speed_2 = 0;
    
    PORTGbits.RG1 = 1; // CCPR4
    PORTGbits.RG2 = 1; // CCPR5
    PORTEbits.RE0 = 1; // ECCP3
    
    //;static unsigned char direction = 0;
    // Read ADC
    pot_pos_1 = Read_ADC(POT_CHAN_1);
    pot_pos_2 = Read_ADC(POT_CHAN_2);
    
    // Convert value to speed (right shift x4)
    speed_1 = pot_pos_1 >> 4;   // check value for pot position in testing for troubleshooting
    speed_2 = pot_pos_2 >> 4;
    
    // Update OC5
    CCPR4 = speed_1 + OFFSET;   // Set duty cycle with offset
    CCPR5 = speed_2 + OFFSET;
    
    // Get train priority
 /*   if (PORTCbits.RC1 > 0){         // Train 1 Priority
        priority = 1;
    }
    if (PORTCbits.RC1 == 0){         // Train 1 Priority
        priority = 2;
    }
  */ 
    VelValue2();
    LCD_GoTo(0,0);
    LCD_WriteString(velocity2,3);
    VelValue1();
    LCD_GoTo(0,13);
    LCD_WriteString(velocity,3);
} 
//determine the outer track display velocity
void VelValue1(void){ 
    int pot_pos_1 = 0;//, speed_1 = 0;
    pot_pos_1 = Read_ADC(POT_CHAN_1);
 if (16<=pot_pos_1 && pot_pos_1<=18){  //--> 0.9in/s
    velocity = "0.8 ";}
else if (19<=pot_pos_1 && pot_pos_1<=21){ //--> 1in/s
    velocity = "0.9 ";}
else if (22<=pot_pos_1 && pot_pos_1<=24){
    velocity = "1.0 ";}
else if (25<=pot_pos_1 && pot_pos_1<=28){
    velocity = "1.1 ";}
else if (29<=pot_pos_1 && pot_pos_1<=31){
    velocity = "1.2 ";}
else if (32<=pot_pos_1 && pot_pos_1<=34){  
    velocity = "1.3 ";}
else if (35<=pot_pos_1 && pot_pos_1<=37){  
    velocity = "1.4 ";}
else if (38<=pot_pos_1 && pot_pos_1<=41){ 
    velocity = "1.5 ";}
else if (42<=pot_pos_1 && pot_pos_1<=44){
    velocity = "1.6 ";}
else if (45<=pot_pos_1 && pot_pos_1<=47){
    velocity = "1.7 ";}
else if (48<=pot_pos_1 && pot_pos_1<=50){
    velocity = "1.8 ";}
else if (51<=pot_pos_1 && pot_pos_1<=54){  
    velocity = "1.9 ";}
else if (55<=pot_pos_1 && pot_pos_1<=58){  
    velocity = "2.0 ";}
else if (59<=pot_pos_1 && pot_pos_1<=61){
    velocity = "2.1 ";}
else if (62<=pot_pos_1 && pot_pos_1<=64){
    velocity = "2.2 ";}
else if (65<=pot_pos_1 && pot_pos_1<=68){
    velocity = "2.3 ";}
else if (69<=pot_pos_1 && pot_pos_1<=71){
    velocity = "2.4 ";}
else if (72<=pot_pos_1 && pot_pos_1<=74){  
    velocity = "2.5 ";}
else if (75<=pot_pos_1 && pot_pos_1<=77){  
    velocity = "2.6 ";}
else if (78<=pot_pos_1 && pot_pos_1<=81){ 
    velocity = "2.7 ";}
else if (82<=pot_pos_1 && pot_pos_1<=84){
    velocity = "2.8 ";}
else if (85<=pot_pos_1 && pot_pos_1<=87){
    velocity = "2.9 ";}
else if (88<=pot_pos_1 && pot_pos_1<=91){
    velocity = "3.0 ";}
else if (82<=pot_pos_1 && pot_pos_1<=95){
    velocity = "3.1 ";}
else if (96<=pot_pos_1 && pot_pos_1<=98){
    velocity = "3.2 ";}
else if (99<=pot_pos_1 && pot_pos_1<=101){  
    velocity = "3.3 ";}
else if (102<=pot_pos_1 && pot_pos_1<=104){  
    velocity = "3.4 ";}
else if (105<=pot_pos_1 && pot_pos_1<=108){ 
    velocity = "3.5 ";}
else if (109<=pot_pos_1 && pot_pos_1<=111){
    velocity = "3.6 ";}
else if (112<=pot_pos_1 && pot_pos_1<=114){
    velocity = "3.7 ";}
else if (115<=pot_pos_1 && pot_pos_1<=117){
    velocity = "3.8 ";}
else if (118<=pot_pos_1 && pot_pos_1<=121){  
    velocity = "3.9 ";}
else if (122<=pot_pos_1 && pot_pos_1<=125){  
    velocity = "4.0 ";}
else if (126<=pot_pos_1 && pot_pos_1<=128){
    velocity = "4.1 ";}
else if (129<=pot_pos_1 && pot_pos_1<=131){
    velocity = "4.2 ";}
else if (132<=pot_pos_1 && pot_pos_1<=135){
    velocity = "4.3 ";}
else if (136<=pot_pos_1 && pot_pos_1<=138){
    velocity = "4.4 ";}
else if (139<=pot_pos_1 && pot_pos_1<=141){  
    velocity = "4.5 ";}
else if (142<=pot_pos_1 && pot_pos_1<=144){  
    velocity = "4.6 ";}
else if (145<=pot_pos_1 && pot_pos_1<=148){ 
    velocity = "4.7 ";}
else if (149<=pot_pos_1 && pot_pos_1<=151){
    velocity = "4.8 ";}
else if (152<=pot_pos_1 && pot_pos_1<=154){
    velocity = "4.9 ";}
else if (155<=pot_pos_1 && pot_pos_1<=158){
    velocity = "5.0 ";}
else if (159<=pot_pos_1 && pot_pos_1<=162){
    velocity = "5.1 ";}
else if (163<=pot_pos_1 && pot_pos_1<=165){
    velocity = "5.2 ";}
else if (166<=pot_pos_1 && pot_pos_1<=168){  
    velocity = "5.3 ";}
else if (169<=pot_pos_1 && pot_pos_1<=171){  
    velocity = "5.4 ";}
else if (172<=pot_pos_1 && pot_pos_1<=175){ 
    velocity = "5.5 ";}
else if (176<=pot_pos_1 && pot_pos_1<=178){
    velocity = "5.6 ";}
else if (179<=pot_pos_1 && pot_pos_1<=181){
    velocity = "5.7 ";}
else if (182<=pot_pos_1 && pot_pos_1<=184){
    velocity = "5.8 ";}
else if (185<=pot_pos_1 && pot_pos_1<=188){  
    velocity = "5.9 ";}
else if (189<=pot_pos_1 && pot_pos_1<=192){  
    velocity = "6.0 ";}
else if (193<=pot_pos_1 && pot_pos_1<=195){
    velocity = "6.1 ";}
else if (196<=pot_pos_1 && pot_pos_1<=198){
    velocity = "6.2 ";}
else if (199<=pot_pos_1 && pot_pos_1<=202){
    velocity = "6.3 ";}
else if (203<=pot_pos_1 && pot_pos_1<=205){
    velocity = "6.4 ";}
else if (206<=pot_pos_1 && pot_pos_1<=208){  
    velocity = "6.5 ";}
else if (209<=pot_pos_1 && pot_pos_1<=211){  
    velocity = "6.6 ";}
else if (212<=pot_pos_1 && pot_pos_1<=215){ 
    velocity = "6.7 ";}
else if (216<=pot_pos_1 && pot_pos_1<=218){
    velocity = "6.8 ";}
else if (219<=pot_pos_1 && pot_pos_1<=221){
    velocity = "6.9 ";}
else if (222<=pot_pos_1 && pot_pos_1<=225){
    velocity = "7.0 ";}
else if (226<=pot_pos_1 && pot_pos_1<=229){
    velocity = "7.1 ";}
else if (230<=pot_pos_1 && pot_pos_1<=232){
    velocity = "7.2 ";}
else if (233<=pot_pos_1 && pot_pos_1<=235){  
    velocity = "7.3";}
else if (236<=pot_pos_1 && pot_pos_1<=238){  
    velocity = "7.4";}
else if (239<=pot_pos_1 && pot_pos_1<=242){ 
    velocity = "7.5";}
else if (243<=pot_pos_1 && pot_pos_1<=245){
    velocity = "7.6";}
else if (246<=pot_pos_1 && pot_pos_1<=249){
    velocity = "7.7";}
else if (250<=pot_pos_1 && pot_pos_1<=255){
    velocity = "7.8";}
else{
    velocity = "--- ";}  
}
//determine the inner track display velocity
void VelValue2(void){
    int pot_pos_2 = 0;//, speed_1 = 0;
    pot_pos_2 = Read_ADC(POT_CHAN_2);
 if (16<=pot_pos_2 && pot_pos_2<=18){  //--> 0.9in/s
    velocity2 = "0.8 ";}
else if (19<=pot_pos_2 && pot_pos_2<=21){ //--> 1in/s
    velocity2 = "0.9 ";}
else if (22<=pot_pos_2 && pot_pos_2<=24){
    velocity2 = "1.0 ";}
else if (25<=pot_pos_2 && pot_pos_2<=28){
    velocity2 = "1.1 ";}
else if (29<=pot_pos_2 && pot_pos_2<=31){
    velocity2 = "1.2 ";}
else if (32<=pot_pos_2 && pot_pos_2<=34){  
    velocity2 = "1.3 ";}
else if (35<=pot_pos_2 && pot_pos_2<=37){  
    velocity2 = "1.4 ";}
else if (38<=pot_pos_2 && pot_pos_2<=41){ 
    velocity2 = "1.5 ";}
else if (42<=pot_pos_2 && pot_pos_2<=44){
    velocity2 = "1.6 ";}
else if (45<=pot_pos_2 && pot_pos_2<=47){
    velocity2 = "1.7 ";}
else if (48<=pot_pos_2 && pot_pos_2<=50){
    velocity2 = "1.8 ";}
else if (51<=pot_pos_2 && pot_pos_2<=54){  
    velocity2 = "1.9 ";}
else if (55<=pot_pos_2 && pot_pos_2<=58){  
    velocity2 = "2.0 ";}
else if (59<=pot_pos_2 && pot_pos_2<=61){
    velocity2 = "2.1 ";}
else if (62<=pot_pos_2 && pot_pos_2<=64){
    velocity2 = "2.2 ";}
else if (65<=pot_pos_2 && pot_pos_2<=68){
    velocity2 = "2.3 ";}
else if (69<=pot_pos_2 && pot_pos_2<=71){
    velocity2 = "2.4 ";}
else if (72<=pot_pos_2 && pot_pos_2<=74){  
    velocity2 = "2.5 ";}
else if (75<=pot_pos_2 && pot_pos_2<=77){  
    velocity2 = "2.6 ";}
else if (78<=pot_pos_2 && pot_pos_2<=81){ 
    velocity2 = "2.7 ";}
else if (82<=pot_pos_2 && pot_pos_2<=84){
    velocity2 = "2.8 ";}
else if (85<=pot_pos_2 && pot_pos_2<=87){
    velocity2 = "2.9 ";}
else if (88<=pot_pos_2 && pot_pos_2<=91){
    velocity2 = "3.0 ";}
else if (82<=pot_pos_2 && pot_pos_2<=95){
    velocity2 = "3.1 ";}
else if (96<=pot_pos_2 && pot_pos_2<=98){
    velocity2 = "3.2 ";}
else if (99<=pot_pos_2 && pot_pos_2<=101){  
    velocity2 = "3.3 ";}
else if (102<=pot_pos_2 && pot_pos_2<=104){  
    velocity2 = "3.4 ";}
else if (105<=pot_pos_2 && pot_pos_2<=108){ 
    velocity2 = "3.5 ";}
else if (109<=pot_pos_2 && pot_pos_2<=111){
    velocity2 = "3.6 ";}
else if (112<=pot_pos_2 && pot_pos_2<=114){
    velocity2 = "3.7 ";}
else if (115<=pot_pos_2 && pot_pos_2<=117){
    velocity2 = "3.8 ";}
else if (118<=pot_pos_2 && pot_pos_2<=121){  
    velocity2 = "3.9 ";}
else if (122<=pot_pos_2 && pot_pos_2<=125){  
    velocity2 = "4.0 ";}
else if (126<=pot_pos_2 && pot_pos_2<=128){
    velocity2 = "4.1 ";}
else if (129<=pot_pos_2 && pot_pos_2<=131){
    velocity2 = "4.2 ";}
else if (132<=pot_pos_2 && pot_pos_2<=135){
    velocity2 = "4.3 ";}
else if (136<=pot_pos_2 && pot_pos_2<=138){
    velocity2 = "4.4 ";}
else if (139<=pot_pos_2 && pot_pos_2<=141){  
    velocity2 = "4.5 ";}
else if (142<=pot_pos_2 && pot_pos_2<=144){  
    velocity2 = "4.6 ";}
else if (145<=pot_pos_2 && pot_pos_2<=148){ 
    velocity2 = "4.7 ";}
else if (149<=pot_pos_2 && pot_pos_2<=151){
    velocity2 = "4.8 ";}
else if (152<=pot_pos_2 && pot_pos_2<=154){
    velocity2 = "4.9 ";}
else if (155<=pot_pos_2 && pot_pos_2<=158){
    velocity2 = "5.0 ";}
else if (159<=pot_pos_2 && pot_pos_2<=162){
    velocity2 = "5.1 ";}
else if (163<=pot_pos_2 && pot_pos_2<=165){
    velocity2 = "5.2 ";}
else if (166<=pot_pos_2 && pot_pos_2<=168){  
    velocity2 = "5.3 ";}
else if (169<=pot_pos_2 && pot_pos_2<=171){  
    velocity2 = "5.4 ";}
else if (172<=pot_pos_2 && pot_pos_2<=175){ 
    velocity2 = "5.5 ";}
else if (176<=pot_pos_2 && pot_pos_2<=178){
    velocity2 = "5.6 ";}
else if (179<=pot_pos_2 && pot_pos_2<=181){
    velocity2 = "5.7 ";}
else if (182<=pot_pos_2 && pot_pos_2<=184){
    velocity2 = "5.8 ";}
else if (185<=pot_pos_2 && pot_pos_2<=188){  
    velocity2 = "5.9 ";}
else if (189<=pot_pos_2 && pot_pos_2<=192){  
    velocity2 = "6.0 ";}
else if (193<=pot_pos_2 && pot_pos_2<=195){
    velocity2 = "6.1 ";}
else if (196<=pot_pos_2 && pot_pos_2<=198){
    velocity2 = "6.2 ";}
else if (199<=pot_pos_2 && pot_pos_2<=202){
    velocity2 = "6.3 ";}
else if (203<=pot_pos_2 && pot_pos_2<=205){
    velocity2 = "6.4 ";}
else if (206<=pot_pos_2 && pot_pos_2<=208){  
    velocity2 = "6.5 ";}
else if (209<=pot_pos_2 && pot_pos_2<=211){  
    velocity2 = "6.6 ";}
else if (212<=pot_pos_2 && pot_pos_2<=215){ 
    velocity2 = "6.7 ";}
else if (216<=pot_pos_2 && pot_pos_2<=218){
    velocity2 = "6.8 ";}
else if (219<=pot_pos_2 && pot_pos_2<=221){
    velocity2 = "6.9 ";}
else if (222<=pot_pos_2 && pot_pos_2<=225){
    velocity2 = "7.0 ";}
else if (226<=pot_pos_2 && pot_pos_2<=229){
    velocity2 = "7.1 ";}
else if (230<=pot_pos_2 && pot_pos_2<=232){
    velocity2 = "7.2 ";}
else if (233<=pot_pos_2 && pot_pos_2<=235){  
    velocity2 = "7.3";}
else if (236<=pot_pos_2 && pot_pos_2<=238){  
    velocity2 = "7.4";}
else if (239<=pot_pos_2 && pot_pos_2<=242){ 
    velocity2 = "7.5";}
else if (243<=pot_pos_2 && pot_pos_2<=245){
    velocity2 = "7.6";}
else if (246<=pot_pos_2 && pot_pos_2<=249){
    velocity2 = "7.7";}
else if (250<=pot_pos_2 && pot_pos_2<=255){
    velocity2 = "7.8";}
else{
    velocity2 = "--- ";}  
}

int BINTOINT (void){
    
    sensorCode[0] = PORTDbits.RD3;
    sensorCode[1] = PORTDbits.RD2;
    sensorCode[2] = PORTDbits.RD1;
    sensorCode[3] = PORTEbits.RE6;
    sensorCode[4] = PORTEbits.RE5;
    sensorCode[5] = PORTEbits.RE4;
    sensorCode[6] = PORTEbits.RE3;
    sensorCode[7] = PORTEbits.RE2;
    
    int sensorCodeDec = sensorCode[0]*2^7 +
                        sensorCode[1]*2^6 +
                        sensorCode[2]*2^5 +
                        sensorCode[3]*2^4 +
                        sensorCode[4]*2^3 +
                        sensorCode[5]*2^2 +
                        sensorCode[6]*2^1 +
                        sensorCode[7]*2^0;
    
    return (sensorCodeDec);
}

void CODE_TEST(void){
    
    int codeDec = BINTOINT();
    
    if (codeDec == 0){            // All sensors triggered
        Code_0();
    }
    else if (codeDec == 63){      // AB
        Code_63();
    }
    else if (codeDec == 95){      // AC
        Code_95();
    }
    else if (codeDec == 119){     // AE
        Code_119();
    }
    else if (codeDec == 125){     // AG
        Code_125();
    }
    else if (codeDec == 127){     // A
        Code_127();
    }
    else if (codeDec == 175){     // BD
        Code_175();
    }
    else if (codeDec == 183){     // BE
        Code_183();
    }
    else if (codeDec == 187){     // BF
        Code_187();
    }
    else if (codeDec == 190){     // BH
        Code_190();
    }
    else if (codeDec == 191){     // B
        Code_191();
    }
    else if (codeDec == 207){     // CD
        Code_207();
    }
    else if (codeDec == 215){     // CE
        Code_215();
    }
    else if (codeDec == 219){     // CF
        Code_219();
    }
    else if (codeDec == 222){     // CH
        Code_222();
    }
    else if (codeDec == 223){     // C
        Code_223();
    }
    else if (codeDec == 231){     // DE
        Code_231();
    }
    else if (codeDec == 237){     // DG
        Code_237();
    }
    else if (codeDec == 239){     // D
        Code_239();
    }
    else if (codeDec == 243){     // EF
        Code_243();
    }
    else if (codeDec == 245){     // EG
        Code_245();
    }
    else if (codeDec == 246){     // EH
        Code_246();
    }
    else if (codeDec == 247){     // E
        Code_247();
    }
    else if (codeDec == 249){     // FG
        Code_249();
    }
    else if (codeDec == 251){     // F
        Code_251();
    }
    else if (codeDec == 252){     // GH
        Code_252();
    }
    else if (codeDec == 253){     // G
        Code_253();
    }
    else if (codeDec == 254){     // H
        Code_254();
    }
    else if (codeDec == 255){     // None
        Code_255();
    }
    else       // Need to explore this and why this could happen / what to do about it. 
        return;
}
