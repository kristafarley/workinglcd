/* 
 * File: adc.h 
 * Author: Dr. Ron Hayne
 * 
 * Revision history: Created 10/8/17
 */

extern void Init_ADC(void);
extern unsigned char Read_ADC(unsigned char Channel_Number); 