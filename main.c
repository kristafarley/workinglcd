/* main.c
 * Notes:
 * "1" and "2" refer to Outer and Inner loops respectively
 */

#include <xc.h>
#include <pic18f87J11.h>
#include "user.h"
#include "adc.h"
#include"sensorCodes.h"
#include "lcdcomm.h"

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void main(void) 
{
    // Init I/O and Peripherals
    Init_App();
    while (1) {
        UPDINT();
        //BINTOINT();
        CODE_TEST();
        //Sensor_Read();
        //Priority_Read();
    }
}

