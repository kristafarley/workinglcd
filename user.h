/* 
 * File: user.h
 * Author: Dr. Ron Hayne
 * 
 * Revision history: Created 10/8/17
 */

/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

// I/O Name Macros

/* Can possibly delete
#define LEDS_D      LATD
#define LEDS_B      LATB
 */

//#define BTN1        PORTBbits.RB0  // Active low input switches
//#define BTN2        PORTAbits.RA5

#define PWM_Signal  LATGbits.LATG3 // PWM Output RG3 - Edited
#define PWM_Bool    LATGbits.LATG1
#define CS          PORTAbits.RA2

//MSSP SPI definitions
#define IO_DIR_A_ADDRESS        0x00
#define OUTPUT_DIR              0x00


/* Application specific user parameters */
#define POT_CHAN_OUTER    (15)
#define POT_CHAN_INNER    (14)
#define PERIOD      (75)
#define OFFSET      (25)

extern unsigned short speed_1;
extern unsigned short speed_2;
extern int priority;
extern int sensorCode[8];
char *velocity;
char *velocity2;

/******************************************************************************/
/* User Function Prototypes                                                  */
/******************************************************************************/
void Init_GPIO(void);
void Init_Timer2(void);
void Init_App(void);
void UPDINT(void);
int BINTOINT (void);
void CODE_TEST(void);
void LCD_Initialize(void);
void VelValue1(void);
void VelValue2(void);

